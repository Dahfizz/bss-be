##!/bin/python3 
import requests
import json
import time 
import mysql.connector
import configparser

config = configparser.ConfigParser()
config.read('/tmp/bng.conf')

db_username = config['DEFAULT']['db-username']
db_password = config['DEFAULT']['db-password']
db_host = config['DEFAULT']['db-host']
db_database = config['DEFAULT']['db-database']
onap_nbi_url = config['DEFAULT']['onap-nbi-url']
onap_dcae_ves_collector_url = config['DEFAULT']['onap-dcae-ves-collector-url']
onap_message_router = config['DEFAULT']['onap-message-router']

while(True):
    try:
      response = requests.get(onap_message_router)	  
      dbcon = mysql.connector.connect(user=db_username,password=db_password,host=db_host,port=3306,database=db_database) 
      json_data = json.loads(response.text)   
      print(json_data)
      print(len(response.text))
      print(response.text)     
      if len(response.text) <= 10:
        print("No valid json")
        time.sleep(20)
        continue
      cur = dbcon.cursor()         
      insert_sql_query = "INSERT INTO dmaap (`event_description`) VALUES (%s)"
      insert_tuple = (json_data)
      result  = cur.execute(insert_sql_query, insert_tuple)
      dbcon.commit()
      cur.close()
      dbcon.close()
      time.sleep(10)
    except mysql.connector.Error as err:
      print("Something went wrong: {}".format(err))
      time.sleep(10)
    except requests.exceptions.HTTPError as errh:
      print ("Http Error:",errh)
      time.sleep(10)
    except requests.exceptions.ConnectionError as errc:
      print ("Error Connecting:",errc)
      time.sleep(10)
    except requests.exceptions.Timeout as errt:
      print ("Timeout Error:",errt)
      time.sleep(10)
    except requests.exceptions.RequestException as err:
      print ("OOps: Something Else",err)
      time.sleep(10)

    
    
#sudo pip3 install mysql-connector-python
#https://github.com/att/dmaap-framework/wiki/Message-Router-API-Call-unit-tests
#https://onap.readthedocs.io/en/beijing/submodules/dmaap/dbcapi.git/docs/api.html
#https://onap.readthedocs.io/en/beijing/submodules/dmaap/messagerouter/messageservice.git/docs/offeredapis/offeredapis.html
#https://mail.openvswitch.org/pipermail/onap-discuss/2018-January/007150.html
#https://www.mail-archive.com/onap-discuss@lists.onap.org/msg09797.html

#filter messages
#https://onap.readthedocs.io/en/beijing/submodules/dmaap/messagerouter/messageservice.git/docs/offeredapis/offeredapis.html#filters
