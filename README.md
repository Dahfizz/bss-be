# BSS Portal Backend

This is a simple python flask app that integrates with ONAP for the demo portal.

Everything interesting is in app.py. That file reads from the database, accepts HTTP
requests, and makes API calls into ONAP to create / delete / track orders.

Information on setting up the front and backend can be found on the ONAP wiki:
https://wiki.onap.org/display/DW/Swisscom+BSS+UI+portal+mockup
