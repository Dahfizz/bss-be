# Copyright 2019 Swisscom Apps Team
# Copyright 2019 Parker Berberian - UNH-IOL - nfvlab@iol.unh.edu
from flask import Flask, request, jsonify
import mysql.connector
import requests
import uuid
from flask import make_response
import configparser

config = configparser.ConfigParser()
config.read('/tmp/bng.conf')

db_username = config['DEFAULT']['db-username']
db_password = config['DEFAULT']['db-password']
db_host = config['DEFAULT']['db-host']
db_database = config['DEFAULT']['db-database']
onap_nbi_url = config['DEFAULT']['onap-nbi-url']
onap_dcae_ves_collector_url = config['DEFAULT']['onap-dcae-ves-collector-url']
onap_message_router = config['DEFAULT']['onap-message-router']

app = Flask(__name__)


@app.route('/dmaapevents', methods=['GET'])
def get_dmaapevents():
    connection = mysql.connector.connect(user=db_username,password=db_password,host=db_host,port=3306, database=db_database)
    cursor = connection.cursor()
    cursor.execute("SELECT event_description FROM dmaap ORDER BY event_id DESC")

    json_payload = '['

    result = cursor.fetchall()
    for row in result:
        for i, value in enumerate(row):
            json_payload += str(value) + ','

    json_payload = json_payload[:-1]
    json_payload += ']'

    connection.commit()
    cursor.close()
    connection.close()
    response = make_response(json_payload)
    response.headers['Content-type'] = 'application/json'
    return response


#TODO: This will have to change depending on the
# hardware you are testing with.
vendors = [
    {
        "name": "Huawei",
        "pnf_name": "Huawei-HWTC02FF269D",
        "serial": "HWTC02FF269D",
        "rgw_mac": "54:04:a6:38:12:9d",
        "s_vlan": "1000",
        "c_vlan": "100",
        "remote_id": "AC9.001.002.003"
    },
    {
        "name": "DZS",
        "pnf_name": "KeyMile-DSNW677FE2B0",
        "serial": "DSNW677FE2B0",
        "rgw_mac": "f8:ca:b8:09:ef:52",
        "s_vlan": "400",
        "c_vlan": "",
        "remote_id": ""
    },
    {
        "name": "Altice",
        "pnf_name": "AlticeLabs-PTIN9170818F",
        "serial": "PTIN9170818F",
        "rgw_mac": "00:06:91:70:81:92",
        "s_vlan": "300",
        "c_vlan": "",
        "remote_id": ""
    },
    {
        "name": "Nokia",
        "pnf_name": "Nokia-ALCLF88A784D",
        "serial": "ALCLF88A784D",
        "rgw_mac": "20:78:52:cd:78:61",
        "s_vlan": "500",
        "c_vlan": "",
        "remote_id": ""
    }
]

# returns all the vendors available
@app.route('/vendors', methods=['GET'])
def get_vendors():
    return jsonify({'vendors': vendors})

# returns all the products to be visualized (S/M/L,etc)
@app.route('/products', methods=['GET'])
def get_producs():
    connection = mysql.connector.connect(user=db_username,password=db_password,host=db_host,port=3306,database=db_database)
    cursor = connection.cursor()
    cursor.execute("SELECT * from products")
    r = [dict((cursor.description[i][0], value)
              for i, value in enumerate(row)) for row in cursor.fetchall()]
    connection.commit()
    cursor.close()
    connection.close()
    return jsonify({'products': r})


# returns all customer, currently not in use
@app.route('/customers', methods=['GET'])
def get_customers():
    connection = mysql.connector.connect(user=db_username,password=db_password,host=db_host, port=3306, database=db_database)
    cursor = connection.cursor()
    cursor.execute("SELECT * from customers")
    r = [dict((cursor.description[i][0], value)
              for i, value in enumerate(row)) for row in cursor.fetchall()]
    connection.commit()
    cursor.close()
    connection.close()
    return jsonify({'customers': r})

# all orders back to the UI, along with state, etc
@app.route('/orders', methods=['GET'])
def orders():
    connection = mysql.connector.connect(user=db_username,password=db_password,host=db_host, port=3306, database=db_database)
    cursor = connection.cursor()
    cursor.execute("SELECT products.product_id, products.product_description, customers.customer_id, customers.customer_description, states.state_id, states.state_description, states.state_text, orders.order_id from products, customers, states, orders, service_orders where products.product_id = orders.product_id AND orders.onap_order_tracking_id=service_orders.onap_order_tracking_id AND customers.customer_id=orders.customer_id AND states.state_id = orders.state_id AND NOT service_orders.deleted order by orders.order_id DESC")
    r = [dict((cursor.description[i][0], value)
              for i, value in enumerate(row)) for row in cursor.fetchall()]
    connection.commit()
    cursor.close()
    connection.close()
    return jsonify({'orders': r})

# get specific order_id
@app.route('/order/<int:orderid>', methods=['GET'])
def orderidget(orderid):
    connection = mysql.connector.connect(user=db_username,password=db_password,host=db_host, port=3306, database=db_database)
    cursor = connection.cursor()
    data = (orderid,)
    cursor.execute("SELECT products.product_id, products.product_description, customers.customer_id, customers.customer_description, states.state_id, states.state_description, states.state_text, orders.order_id from products, customers, states, orders where products.product_id = orders.product_id AND customers.customer_id=orders.customer_id AND states.state_id = orders.state_id AND orders.order_id = %s", data)
    r = [dict((cursor.description[i][0], value)
              for i, value in enumerate(row)) for row in cursor.fetchall()]
    connection.commit()
    cursor.close()
    connection.close()
    return jsonify({'orders': r})




order_details = {
    "OrderID": "AA1123",
    "CustomerID": "BBSCustomer",
    "BundleProductOffer": "INTERNETL",
    "Status": {
        "TpRegisterCPE": "[Completed]",
        "TpSendCPE": "[Completed]",
        "CFSHighSpeedInternetAccess": {
            "status": "cfsstatus",
            "Type": "HSIAFiberResidential",
            "Speed": "10\/10Gbps",
            "TemplateID": "7287fa84-71c9-4d7f-8f10-d9265ee56fbb",
            "ServiceInstanceID": "567fg56g-45rf-4fde-a40c-42f22a02834c"
        },
        "CFSEmail": "[Completed]",
        "CFSFirewall": "[Completed]",
        "TpCustomerNotify": "[Pending]"
    },
    "CPE": {
        "Type": "InternetBox3",
        "Vendor": "Huawei-HWTCC01B7503",
        "SWversion": "1.0.0",
        "MAC": "54:04:a6:38:12:9d",
        "SN": "HWTCC01B7503"
    }
}

# get specific order_id with details
@app.route('/order/<int:orderid>/details/', methods=['GET'])
def orderidgetdetails(orderid):
    connection = mysql.connector.connect(user=db_username,password=db_password,host=db_host, port=3306, database=db_database)
    cursor = connection.cursor()
    data = (orderid,)

    cursor.execute("SELECT orders.onap_order_tracking_id, products.product_id, products.product_description, customers.customer_id, customers.customer_description, states.state_id, states.state_description, states.state_text, orders.order_id, orders.service_instance_id, orders.pnf_name from products, customers, states, orders where products.product_id = orders.product_id AND customers.customer_id=orders.customer_id AND states.state_id = orders.state_id AND orders.order_id = %s", data)
    result = cursor.fetchall()
    for row in result:
        for i, value in enumerate(row):
            print("%s      %s" % (i, value))
            if i == 0:
                order_details['OrderID'] = value
            if i == 2:
                order_details['BundleProductOffer'] = value
            if i == 6:
                order_details['Status']['CFSHighSpeedInternetAccess']['status'] = value
            if i == 9:
                order_details['Status']['CFSHighSpeedInternetAccess']['ServiceInstanceID'] = value

    print(order_details)
    cursor.close()
    connection.close()
    return jsonify({'orders_details' : order_details})

# These two methods are hacks, probably don't use them
# since real order tracking works now
# move_cpe
@app.route('/movecpe/<int:orderid>', methods=['GET'])
def movecpe(orderid):
    return jsonify({'status' : 'ok'})


# plug_cpe
@app.route('/plugcpe/<int:orderid>', methods=['GET'])
def plugcpe(orderid):
    return jsonify({'status' : 'ok'})

# </hacks>

#this is registered and called from ONAP NBI
#POST
#http://onap_nbi:30274/nbi/api/v4/hub
#{"callback": "http://bbsportalhost:port/serviceOrderStateListener/listener/v1/","query":"eventType = ServiceOrderStateChangeNotification"}

@app.route('/serviceOrderStateListener/listener/v1/', methods=['POST'])
def service_Order_State_Listener():
    event_body = request.get_json()

    pnf_name = 'null'

    event_type = event_body['eventType']
    event_id = event_body['event']['id']
    event_state = event_body['event']['state']
    service_instance_id = 'null'

    # ServiceOrderCreationNotification        -> acknowledged
    # ServiceOrderItemStateChangeNotification -> acknowledged
    # ServiceCreationNotification             -> inprogress
    # ServiceAttributeValueChangeNotification -> inprogress
    # ServiceOrderItemStateChangeNotification -> inprogress
    # ServiceOrderStateChangeNotification     -> completed
    # ServiceAttributeValueChangeNotification -> inprogress
    # ServiceAttributeValueChangeNotification -> inprogress
    # ServiceAttributeValueChangeNotification -> inprogress
    # ServiceAttributeValueChangeNotification -> inprogress
    # ServiceAttributeValueChangeNotification -> assigned
    # ServiceOrderItemStateChangeNotification -> inprogress
    # ServiceOrderStateChangeNotification     -> completed
    # ServiceAttributeValueChangeNotification -> active

    if event_type == 'ServiceCreationNotification':
        service_instance_id = event_body['event']['id']

    if event_type == 'ServiceAttributeValueChangeNotification':
        service_instance_id = event_body['event']['id']

    if event_type == 'ServiceOrderStateChangeNotification':
        service_instance_id = event_body['event']['id']
        event_state = 'INPROGRESS'

    if event_state is None:
        event_state = 'INPROGRESS'

    if event_state.upper() == 'ACKNOWLEDGED':
        state_id = 1
    elif event_state.upper() == 'INPROGRESS':
        state_id = 2
    elif event_state.upper() == 'ASSIGNED':
        state_id = 2
    elif event_state.upper() == 'COMPLETED':
        state_id = 2
    elif event_state.upper() == 'ACTIVE':
        state_id = 3
    elif event_state.upper() == 'FAILED':
        state_id = 4
    elif event_state.upper() == 'REJECTED':
        state_id = 5
    else:
        state_id = 2
    print(event_body)
    print(event_type)
    print(event_state)
    print(event_id)

    insert_sql_query = "UPDATE orders SET state_id = %s, ts = UTC_TIMESTAMP(), service_instance_id = %s, pnf_name = %s WHERE (onap_order_tracking_id = service_instance_id OR onap_order_tracking_id = %s OR service_instance_id = %s OR service_instance_id is NULL OR service_instance_id = 'null') "
    data = (state_id, service_instance_id, pnf_name, event_id, service_instance_id,)
    connection = mysql.connector.connect(user=db_username,password=db_password,host=db_host, port=3306, database=db_database)
    cursor = connection.cursor()
    cursor.execute(insert_sql_query, data)
    connection.commit()
    cursor.close()
    connection.close()
    return 'ok'

# noPNF 2.0: 1c45a7d8-6a16-4095-b31f-7da1eb62f2e2
# noPNF 3.0: e9cef376-a80c-485f-83b8-0dd2e84a2503
# noPNF 4.0: 80f4f695-f603-479f-ad19-c77d9e32b1cf
# wPNF 1.0: 0187be8c-8efb-4531-97fa-dbe984ed9cdb // working demo version


# this is the skeleton for the request that
# orders a new service
order_message = {
    "externalId": "BBS_BSS_TrackindId3584",
    "category": "NetworkService",
    "description": "Service Order for a new HSIA CFS",
    "priority": "1",
    "relatedParty": [{
        "id": "f94c7606-31ef-4aef-8305-4a3cdc32ae18",
        "role": "ONAPcustomer",
        "name": "BBSCustomer",
        "@referredType": "Consumer"
    }],
    "orderItem": [
        {
            "id": "1",
            "action": "add",
            "service": {
                "name": "BBS_E2E_Service_1",
                "serviceState": "active",
                "serviceSpecification": {"id": "7287fa84-71c9-4d7f-8f10-d9265ee56fbb"},
                "serviceCharacteristic": [
                    {
                        "name": "ont_ont_swVersion",
                        "value": {"serviceCharacteristicValue": "1.0.0"}
                    },
                    {
                        "name": "edgeinternetprofile_ip_service_type",
                        "value": {"serviceCharacteristicValue": "BBS_E2E_Service"}
                    },
                    {
                        "name": "ont_ont_nf_type",
                        "value": {"serviceCharacteristicValue": "HN8255WS"}
                    },
                ]
            }
        }
    ]
}

# insert a new order into the DB and issue a request against ONAP NBI e.g. serviceOrder
@app.route('/insertorder', methods=['POST'])
def insert_order():
    connection = mysql.connector.connect(user=db_username,password=db_password,host=db_host,port=3306, database=db_database)
    nbi = onap_nbi_url + 'serviceOrder'
    new_order = request.get_json()

    # These are the keys we need from the frontend user to create an
    # order with the correct parameters
    keys = ["ont_vendor", "ont_serial_number", "rgw_mac", "s_vlan", "c_vlan", "up_speed", "down_speed", "remote_id", "product_id", "pnf_name"]
    for k in keys:
        if k not in new_order:
            print("Got bad request")
            print(new_order)
            raise Exception("Bad Request")

    order_json = dict(order_message)  # copy template

    # add the custom values to the order request
    service_attrs = [
        {
            "name": "ont_ont_manufacturer",
            "value": {"serviceCharacteristicValue": 'HUAWEI'}
        },
        {
            "name": "ont_ont_mac_addr",
            "value": {"serviceCharacteristicValue": new_order['rgw_mac']}
        },
        {
            "name": "edgeinternetprofile_ip_remote_id",
            "value": {"serviceCharacteristicValue": new_order['remote_id']}
        },
        {
            "name": "ont_ont_serial_num",
            "value": {"serviceCharacteristicValue": new_order['ont_serial_number']}
        },
        {
            "name": "edgeinternetprofile_ip_rg_mac_addr",
            "value": {"serviceCharacteristicValue": new_order['rgw_mac']}
        },
        {
            "name": "svlan",
            "value": {"serviceCharacteristicValue": new_order['s_vlan']}
        },
        {
            "name": "cvlan",
            "value": {"serviceCharacteristicValue": new_order['c_vlan']}
        },
        {
            "name": "edgeinternetprofile_ip_upstream_speed",
            "value": {"serviceCharacteristicValue": new_order['up_speed']}
        },
        {
            "name": "edgeinternetprofile_ip_downstream_speed",
            "value": {"serviceCharacteristicValue": new_order['down_speed']}
        },
        {
            "name": "ont_ont_pnf_name",
            "value": {"serviceCharacteristicValue": new_order['pnf_name']}
        },
    ]

    for a in service_attrs:
        order_json['orderItem'][0]['service']['serviceCharacteristic'].append(a)

    # give an ID we can track
    order_json["externalId"] = 'BBS_BSS_TrackindId' + str(uuid.uuid4())
    service_name = 'BBS_E2E_Service_' + str(uuid.uuid4())
    order_json["orderItem"][0]["service"]["name"] = service_name
    print(order_json)

    headers = {'Content-type': 'application/json'}
    response = requests.post(nbi, json=order_message, headers=headers)
    order_id_response = response.json()

    insert_sql_query = "INSERT INTO orders (`customer_id`, `state_id`,`product_id`,`onap_order_tracking_id`) VALUES (1,1,%s,%s)"
    data = (new_order['product_id'], order_id_response['id'],)
    cursor = connection.cursor()
    cursor.execute(insert_sql_query, data)
    connection.commit()
    cursor.close()

    insert_sql_query = "INSERT INTO service_orders (`onap_order_tracking_id`, `service_name`,`deleted`) VALUES (%s,%s,false)"
    data = (order_id_response['id'], service_name)
    cursor = connection.cursor()
    cursor.execute(insert_sql_query, data)
    connection.commit()
    cursor.close()

    mycursor = connection.cursor()
    mycursor.execute("select max(order_id) from orders")
    myresult = mycursor.fetchall()
    for x in myresult:
        print(x)
    mycursor.close()
    connection.close()
    return jsonify({'orderid': x})

# deletes a service order based on the order_id
@app.route('/deleteorder', methods=['POST'])
def delete_order():
    connection = mysql.connector.connect(user=db_username,password=db_password,host=db_host,port=3306, database=db_database)
    req_json = request.get_json()
    order_id = req_json['id']

    query_sql = "SELECT service_name FROM service_orders, orders WHERE orders.onap_order_tracking_id=service_orders.onap_order_tracking_id AND orders.order_id=%s"
    data = (order_id,)
    cursor = connection.cursor()
    cursor.execute(query_sql, data)
    service_name = cursor.fetchone()[0]
    cursor.close()

    query_sql = "UPDATE service_orders SET deleted = true WHERE service_name = %s"
    data = (service_name,)
    cursor = connection.cursor()
    cursor.execute(query_sql, data)
    connection.commit()
    cursor.close()

    print("service name:")
    print(service_name)

    #url = "http://172.30.0.78:32154/service-orders/" + service_name
    #headers = {'Content-type': 'application/json'}
    #response = requests.delete(url, headers=headers)

    return jsonify({'response': response.text})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5010, debug=True)
