#!/bin/bash

# This will start the gunicorn workers
python3 /usr/local/bin/gunicorn -w 2 -b 0.0.0.0:5010 --access-logfile ./access.log --error-logfile ./error.log app:app --daemon
